
package stocktracker;

import stocktracker.stockdatamodel.PhysicalStockItem;
import stocktracker.stockdatamodel.ServiceStockItem;
import stocktracker.stockdatamodel.StockItem;
import stocktracker.stockdatamodel.StockType;


public class StockTracker {


    public static void main(String[] args) {
        
        StockItem objTestItem1 = new PhysicalStockItem("Grand Theft Auto V");
        StockItem objTestItem2 = new PhysicalStockItem("Cyberpunk 2077");
        StockItem objTestItem3 = new ServiceStockItem("Delivery");
        
        if (objTestItem1.getItemType() == StockType.PHYSICALITEM){
            System.out.println("Item 1 is a PHYSICAL stock item");
        }else{
            System.out.println("Item 1 is a SERVICE stock item");
        }
        
        if (objTestItem2.getItemType() == StockType.PHYSICALITEM){
            System.out.println("Item 2 is a PHYSICAL stock item");
        }else{
            System.out.println("Item 2 is a SERVICE stock item");
        }
        
        if (objTestItem3.getItemType() == StockType.PHYSICALITEM){
            System.out.println("Item 3 is a PHYSICAL stock item");
        }else{
            System.out.println("Item 3 is a SERVICE stock item");
        }
        
    }
    
}
